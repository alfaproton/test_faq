<?php

/**
 * Date: 06.02.2015
 * Time: 14:03
 */
class Test_Faq_Block_Adminhtml_Faq extends Mage_Adminhtml_Block_Widget_Grid_Container{

    public function __construct(){
        $this->_blockGroup = 'test_faq';
        $this->_controller = 'adminhtml_faq';
        $this->_headerText = $this->__('Answers and Questions');
        $this->_addButtonLabel = $this->__('Add AQ');
        parent::__construct();
    }

    public function getCreateUrl(){
        return $this->getUrl('*/*/new');
    }
}

