<?php

/**
 * Date: 06.02.2015
 * Time: 14:03
 */
class Test_Faq_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct(){
        parent::__construct();
        $this->setId('grid_id');
        $this->setDefaultSort('faq_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('test_faq/faq')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){

        $this->addColumn('faq_id',
            array(
                'header' => $this->__('Faq Id'),
                'width' => '50px',
                'index' => 'faq_id'
            )
        );
        $this->addColumn('question',
            array(
                'header' => $this->__('Question'),
                'index' => 'question'
            )
        );
        $this->addColumn('answer',
            array(
                'header' => $this->__('Answer'),
                'index' => 'answer'
            )
        );
        $this->addColumn('is_active',
            array(
                'header' => $this->__('Is Active'),
                'index' => 'is_active',
                'type' => 'options',
                'options' => array(0 => $this->__('No'), 1 => $this->__('Yes')),
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row){
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction(){
        $modelPk = Mage::getModel('test_faq/faq')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
