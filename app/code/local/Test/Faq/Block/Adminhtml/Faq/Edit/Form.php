<?php

/**
 * Date: 06.02.2015
 * Time: 14:03
 */
class Test_Faq_Block_Adminhtml_Faq_Edit_Form extends Mage_Adminhtml_Block_Widget_Form{

    protected function _getModel(){
        return Mage::registry('current_faq');
    }

    protected function _getHelper(){
        return Mage::helper('test_faq');
    }

    protected function _getModelTitle(){
        return 'Answer and Question';
    }

    protected function _prepareForm(){
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $this->_getHelper()->__("$modelTitle Edit"),
            'class' => 'fieldset-wide',
        ));

        if($model && $model->getId()){
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));
        }
        $fieldset->addField('question', 'text', array(
            'name' => 'question',
            'label' => $this->_getHelper()->__('Edit question text'),
            'title' => $this->_getHelper()->__('Question'),
            'required' => true,
        ));
        $fieldset->addField('answer', 'editor', array(
            'name' => 'answer',
            'label' => $this->_getHelper()->__('Edit Answer'),
            'title' => $this->_getHelper()->__('Answer'),
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
        ));
        $fieldset->addField('is_active', 'select', array(
            'name' => 'is_active',
            'label' => $this->_getHelper()->__('Set does AQ active'),
            'title' => $this->_getHelper()->__('Is Active'),
            'options' => array(0 => 'Not active', 1 => 'Active'),
            'required' => true,
        ));

        if($model){
            $form->setValues($model->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
