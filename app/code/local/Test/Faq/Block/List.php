<?php

/**
 * Date: 06.02.2015
 * Time: 16:11
 */
class Test_Faq_Block_List extends Mage_Core_Block_Template{
    public $defaultRandomQty = 5;

    public function getItems(){
        $items = Mage::getResourceModel('test_faq/faq_collection')->addFieldToFilter('is_active', '1')->getItems();
        if($this->getData('random')){
            shuffle($items);
            $limit = $this->getData('limit') ? $this->getData('limit') : $this->defaultRandomQty;
            return array_slice($items, 0, $limit);
        }
        return $items;
    }

}