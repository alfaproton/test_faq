<?php

class Test_Faq_Block_Widgetlist extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface{
    public function getItems(){
        $idsString = $this->getFaqIds();
        $ids = explode(',', $idsString);
        if(count($ids) > 0){
            $items = Mage::getResourceModel('test_faq/faq_collection')
                ->addFieldToFilter('is_active', '1')
                ->addFieldToFilter('faq_id', array('in' => $ids))
                ->getItems();
            return $items;
        } else{
            return array();
        }
    }
}