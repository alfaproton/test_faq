<?php
/**
 * Date: 06.02.2015
 * Time: 13:00
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('test_faq/faq'))
    ->addColumn('faq_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Faq Id')
    ->addColumn('question', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false,
    ), 'Question')
    ->addColumn('answer', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Answer')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default' => '1',
    ), 'Is Active');
$installer->getConnection()->createTable($table);
$installer->endSetup();