<?php
/**
 * Date: 06.02.2015
 * Time: 13:00
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$testQuestions = array(
    array('question' => 'Question1?', 'answer' => 'Answer1', 'is_active' => true),
    array('question' => 'Question2?', 'answer' => 'Answer2', 'is_active' => false),
    array('question' => 'Question3?', 'answer' => 'Answer3', 'is_active' => true),
);
$faqModel = Mage::getModel('test_faq/faq');
foreach($testQuestions as $testQuestion){
    $faqModel->setData($testQuestion)->save();
}