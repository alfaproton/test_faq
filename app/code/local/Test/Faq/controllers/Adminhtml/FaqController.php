<?php

/**
 * Date: 06.02.2015
 * Time: 13:58
 */
class Test_Faq_Adminhtml_FaqController extends Mage_Adminhtml_Controller_Action{

    public function indexAction(){
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('test_faq/adminhtml_faq'));
        $this->renderLayout();
    }

    public function exportCsvAction(){
        $fileName = 'faq_export.csv';
        $content = $this->getLayout()->createBlock('test_faq/adminhtml_faq_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction(){
        $fileName = 'faq_export.xml';
        $content = $this->getLayout()->createBlock('test_faq/adminhtml_faq_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction(){
        $ids = $this->getRequest()->getParam('ids');
        if(!is_array($ids)){
            $this->_getSession()->addError($this->__('Please select Answer and Question(s).'));
        } else{
            try{
                foreach($ids as $id){
                    $model = Mage::getSingleton('test_faq/faq')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch(Mage_Core_Exception $e){
                $this->_getSession()->addError($e->getMessage());
            } catch(Exception $e){
                $this->_getSession()->addError(
                    Mage::helper('test_faq')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction(){

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('test_faq/faq');

        if($id){
            $model->load($id);
            if(!$model->getId()){
                $this->_getSession()->addError(
                    Mage::helper('test_faq')->__('This Answer and Question no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if(!empty($data)){
            $model->setData($data);
        }

        Mage::register('current_faq', $model);
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('test_faq/adminhtml_faq_edit'));
        if(Mage::getSingleton('cms/wysiwyg_config')->isEnabled()){
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    public function newAction(){
        $this->_forward('edit');
    }

    public function saveAction(){
        $redirectBack = $this->getRequest()->getParam('back', false);
        if($data = $this->getRequest()->getPost()){

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('test_faq/faq');
            if($id){
                $model->load($id);
                if(!$model->getId()){
                    $this->_getSession()->addError(
                        Mage::helper('test_faq')->__('This Answer and Question no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }
            try{
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('test_faq')->__('The Answer and Question has been saved.')
                );
            } catch(Mage_Core_Exception $e){
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch(Exception $e){
                $this->_getSession()->addError(Mage::helper('test_faq')->__('Unable to save the Answer and Question.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if($redirectBack){
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction(){
        if($id = $this->getRequest()->getParam('id')){
            try{
                $model = Mage::getModel('test_faq/faq');
                $model->load($id);
                if(!$model->getId()){
                    Mage::throwException(Mage::helper('test_faq')->__('Unable to find a Answer and Question to delete.'));
                }
                $model->delete();
                $this->_getSession()->addSuccess(
                    Mage::helper('test_faq')->__('The Answer and Question has been deleted.')
                );
                $this->_redirect('*/*/index');
                return;
            } catch(Mage_Core_Exception $e){
                $this->_getSession()->addError($e->getMessage());
            } catch(Exception $e){
                $this->_getSession()->addError(
                    Mage::helper('test_faq')->__('An error occurred while deleting Answer and Question data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        $this->_getSession()->addError(
            Mage::helper('test_faq')->__('Unable to find a Answer and Question to delete.')
        );
        $this->_redirect('*/*/index');
    }
}