<?php
/**
 * Date: 06.02.2015
 * Time: 13:02
  */ 
class Test_Faq_Model_Resource_Faq_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('test_faq/faq');
    }

}